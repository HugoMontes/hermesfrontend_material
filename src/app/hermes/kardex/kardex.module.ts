import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatTableModule } from '@angular/material';
import { MatFormFieldModule, MatPaginatorModule } from '@angular/material';
import { MatInputModule } from '@angular/material';

import { KardexRoutingModule } from './kardex-routing.module';
import { KardexComponent } from './kardex.component';

@NgModule({
    imports: [
        CommonModule,
        KardexRoutingModule,
        MatTableModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatInputModule
    ],
    declarations: [KardexComponent]
})
export class KardexModule {}
